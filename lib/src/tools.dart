part of c7orm;

//TODO: move to tools
class AnnotatedMember {
    Symbol symbol;
    MethodMirror mirror;
    List<InstanceMirror> metadata;

    static List<AnnotatedMember> annotatedInstanceMembers(ClassMirror classMirror){
      var metadatas = new Map<Symbol, List<InstanceMirror>>();
      var mirror = classMirror;
      while (mirror != null) {
        mirror.declarations.forEach((Symbol fieldName, DeclarationMirror mirror) {
          if (mirror.metadata.length > 0) {
            metadatas[fieldName] = mirror.metadata;
          }
        });
        mirror = mirror.superclass;
      }
      var ret = new List<AnnotatedMember>();
      classMirror.instanceMembers.forEach((Symbol fieldName, MethodMirror mirror){
        var r = new AnnotatedMember();
        r.symbol = mirror.simpleName;
        r.mirror = mirror;
        r.metadata = metadatas.containsKey(fieldName) ? metadatas[fieldName] : mirror.metadata;
        ret.add(r);
      });
      return ret;
    }

    static List<VariableMirror> getInstanceVariables(ClassMirror classMirror){
      var ret = new List<VariableMirror>();
      var mirror = classMirror;
      while (mirror != null) {
        mirror.declarations.forEach((Symbol fieldName, DeclarationMirror mirror) {
          if (mirror is VariableMirror) {
            if (!mirror.isStatic) {
              ret.add(mirror);
            }
          }
        });
        mirror = mirror.superclass;
      }
      return ret;

    }

}



/*
 * ALL SUBCLASSES:
 *     final mirror = currentMirrorSystem();
     mirror.libraries.forEach((uri, libMirror){
       //print(uri);
       libMirror.declarations.forEach((name, DeclarationMirror declarationMirror) {
         if((declarationMirror is VariableMirror) && (declarationMirror.type is ClassMirror)){
           ClassMirror parentMirror = declarationMirror.type;
           do {
             if (parentMirror.qualifiedName == #c7.Collection){
               //print('C');
               //print(declarationMirror.runtimeType);
               //force calling constructor
               Collection c = libMirror.getField(declarationMirror.simpleName).reflectee;
               ret[MirrorSystem.getName(c._mirror.qualifiedName).replaceAll('.', '_')] = this.getTableSchema(c._mirror);

             }
           } while ((parentMirror = parentMirror.superclass) != null);
         }
         if(declarationMirror is ClassMirror){
           try{

             ClassMirror parentMirror = declarationMirror;
             while ((parentMirror = parentMirror.superclass) != null){
               if (MirrorSystem.getName(parentMirror.qualifiedName) == "c7.Entity")
               {
                 print(name);
                 //print(classMirror is ClassMirror);
                 //print(classMirror.superclass);
                 classMirror.invoke(#getTableSchema, [classMirror]);

                 classMirror.declarations.forEach((name, DeclarationMirror declarationMirror) {
                 print(declarationMirror);
                 });
               }
               //print(classMirror.simpleName.toString());
             }

           } catch(e){
             print(e);
           }
         }
*/
