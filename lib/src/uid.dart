/**
 *  Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.
 */

part of c7orm;

class Field {}

class Uid implements Field {

  @PrimaryKey
  @AutoIncrement
  int _;

  Uid ();

  Uid.fromInt (this._);

  Uid.fromString (String code) {
    this._ = 10;
  }

  toString() => "U$_";

}
