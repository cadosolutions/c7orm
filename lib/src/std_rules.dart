// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

part of c7orm;

//TODO: remove this after non nullable types will be added to Dart!
class NotNullRule implements Rule<Object> {
  const NotNullRule();

  void validate(Validation validator, Object value){
    validator.addErrorIf(value != null, msg: "This field is required");
  }
}
const NotNull = const NotNullRule();

class Min implements Rule<Comparable> {
  final Comparable x;
  const Min(this.x);

  void validate(Validation validator, Comparable value){
    validator.addErrorIf(value.compareTo(x) > 0, msg: "{name} should be greater or equal to ${x}");
  }
}

class Max implements Rule<Comparable> {
  final Comparable x;
  const Max(this.x);

  void validate(Validation validator, Comparable value){
    validator.addErrorIf(value.compareTo(x) < 0, msg: "{name} should be less or equlal to ${x}");
  }
}

class IsTrue implements Rule<bool> {
  final String message;
  const IsTrue(this.message);

  void validate(Validation validator, bool value){
    validator.addErrorIf(!value, msg: message);
  }
}

/*
class MaxLength extends Rule<String> {
  final int x;
  const MaxLength(this.x);

  bool isValid(String value){
    return value.length <= x;
  }
}

class IsEqualTo extends Rule<Object> {
  final Object desiredValue;

  const IsEqualTo(this.desiredValue);

  bool isValid(Object value){
    return value == desiredValue;
  }
}


class IsTrueRule extends Rule<Bool> {

  const IsTrueRule();

  bool isValid(Bool value){
    return value == true;
  }
}

const IsTrue = const IsTrueRule();



class DoesNotContainEmailRule extends Rule<String> {

  const IsTrueRule();

  bool isValid(String text){
    if text.indexOf("email");

    return value == true;
  }
}

const DoesNotContainEmail = const DoesNotContainEmailRule();
*/
