// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

part of c7orm;


class Validation {

  var validatee;
  List<String> errors = new List<String>();
  List<String> warnings = new List<String>();
  var suggested; // value that passes more or even all tests
  Map<String, Validation> fields = new Map<String, Validator>();

  Map<Symbol, List<InstanceMirror>> getDeclarationsMetadata(ClassMirror mirror){
    var ret = new Map<Symbol, List<InstanceMirror>>();
    mirror.declarations.forEach((Symbol fieldName, DeclarationMirror mirror) {
      if (mirror.metadata.length > 0) {
        ret[fieldName] = mirror.metadata;
      }
    });
    if (mirror.superclass != null)
      ret.addAll(getDeclarationsMetadata(mirror.superclass));
    return ret;
  }
  
  Validation(this.validatee, [DeclarationMirror declaration]) {
    //TODO use T
    InstanceMirror instanceMirror = reflect(this.validatee);
    //reflectClass(T).metadata
    var declarationsMetadata = getDeclarationsMetadata(instanceMirror.type);

    instanceMirror.type.instanceMembers.forEach((Symbol fieldSymbol, MethodMirror mirror) {
        List<InstanceMirror> meta;
        if (mirror.isSynthetic && declarationsMetadata.containsKey(fieldSymbol)) {
          //getters and setters metadata can be only accessed via declarations
          meta = declarationsMetadata[fieldSymbol];
        } else {
          meta = mirror.metadata;
        }
        String fieldName = MirrorSystem.getName(mirror.simpleName);

        if (mirror.isGetter) { //can be read?
          print('MEMBER ${fieldName} ${mirror.returnType} ${mirror.isSynthetic}');
          //TODO validate type in production mode.
          List<InstanceMirror> rules = new List<InstanceMirror>();
          meta.forEach((InstanceMirror metadata) {
            print(metadata.type.superinterfaces);
            rules.add(metadata);
          });
          if (!rules.isEmpty) {
            print("going into $rules");
            fields[fieldName] = new Validation(instanceMirror.getField(fieldSymbol).reflectee, mirror);
          }
        }
    });
    /*
    reflectClass(T).declarations.forEach((fieldName, DeclarationMirror mirror) {
      mirror.metadata.forEach((InstanceMirror meta){
        String key = MirrorSystem.getName(mirror.simpleName);
        print("${key} = ${instanceMirror.getField(fieldName).reflectee}");
        //print(meta.toString());
        if (meta.reflectee is Rule) {
            print(meta.reflectee.validate(this, instanceMirror.getField(fieldName).reflectee));
        }
      });
    });*/
  }

  void addErrorIf(bool condition, {String msg}) {
    if (!condition) {
      errors.add(msg);
    }
  }

  bool isValid() {
      return true;
  }

}
