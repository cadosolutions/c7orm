// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

part of c7orm;

class Migrator {

  static Future<List<String>> quessMigrationQueries() async {
    var ret = new List<String>();
    for (var symbol in Collection.getAllCollections.keys) {
      print("migrating `$symbol`...");

      var newTableColumns = new Map<String, String>();
      var newTableKeysMap = new Map<AbstractKey, List<String>>();
      for (var field in Collection.getAllCollections[symbol].controller.allFields) {
        if (field.hasDbColumn) {
          newTableColumns[field.getColumnName] = field.getDbColumnDef;
          for (var k in field.getKeys){
            if (!newTableKeysMap.containsKey(k)) {
              newTableKeysMap[k] = new List<String>();
            }
            newTableKeysMap[k].add(field.getColumnName);
          }
        }
      }
      var newTableKeys = new Map<AbstractKey, String>();
      newTableKeysMap.forEach((AbstractKey key, List<String> list){
        newTableKeys[key] = list.map((e)=> '`' + e + '`').join(',');
      });
      var tableName = MirrorSystem.getName(symbol).replaceAll('.', '_');
      String createString;
      try {
        var result = await Collection.getAllCollections[symbol].db.query('SHOW CREATE TABLE `${tableName}`;');
        createString = (await result.first)[1];
      } catch (e) {
        if (e is MySqlException) {
          String query = 'CREATE TABLE `${tableName}` (';
          var bits = new List<String>();
          newTableColumns.forEach((columnName, columnDef){
            bits.add('`${columnName}` ${columnDef}');
          });
          newTableKeys.forEach((key, keyDef){
            bits.add(key.dbDef(keyDef));
          });
          //query += 'PRIMARY KEY (`uid`)';
          query += bits.join(',');
          query += ') ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;';
          ret.add(query);
          continue;
        } else {
          throw e;
        }
      }

      Map<String, String> currentColumns = {};
      Map<String, String> currentKeys = {};
      createString = createString.replaceFirst('CREATE TABLE `${tableName}` (', '');
      createString = createString.replaceFirst(') ENGINE=InnoDB DEFAULT CHARSET=utf8', '');
      List<String> currentDefs = createString.split(',\n').map((v){return v.trim();});
      for (var def in currentDefs) {
        String name = def.startsWith('PRIMARY KEY') ? 'PRIMARY_KEY' : def.split('`')[1];
        if (def.startsWith('PRIMARY KEY') || def.startsWith('UNIQUE KEY') || def.startsWith('KEY')) {
          currentKeys[name] = def;
        } else {
          currentColumns[name] = def.split('`')[2].trim();
        }
      };

      List<String> alters = [];
      newTableColumns.forEach((columnName, columnDef){
        if(currentColumns.containsKey(columnName)){
          if(currentColumns[columnName] != columnDef){
            alters.add('CHANGE `${columnName}` `${columnName}` ${columnDef} /* currently: ${currentColumns[columnName]} */');
          }
          currentColumns.remove(columnName);
        } else {
          alters.add('ADD `${columnName}` ${columnDef}');
        }
      });
      currentColumns.forEach((columnName, columnDef){
        alters.add('DROP COLUMN `${columnName}` /*old: ${columnDef}*/');
      });
      newTableKeys.forEach((AbstractKey key, keyDef){
        if(currentKeys.containsKey(key.stringName)){
          if(currentKeys[key.stringName] != key.dbDef(keyDef)){
            alters.add('DROP ' + (key.stringName == 'PRIMARY_KEY' ? 'PRIMARY KEY' : 'KEY `${key.stringName}`') + ' /* currently: ${currentKeys[key.name]} */');
            alters.add('ADD ${key.dbDef(keyDef)}');
          }
          currentKeys.remove(key.stringName);
        } else {
          alters.add('ADD ${key.dbDef(keyDef)}');
        }
      });
      currentKeys.forEach((keyName, keyDef){
        alters.add('DROP ' + (keyName == 'PRIMARY_KEY' ? 'PRIMARY KEY' : 'KEY `${keyName}`') + ' /* currently: ${keyDef} */');
      });

      if (!alters.isEmpty) {
        ret.add('ALTER TABLE `${tableName}` ' + alters.join(',\n') + ';');
      }

    };
    return ret;
  }
}
