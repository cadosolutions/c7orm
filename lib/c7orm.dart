// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

library c7orm;

import 'dart:mirrors';
import 'dart:async';
import 'package:sqljocky/sqljocky.dart';

part 'src/tools.dart';
part 'src/rule.dart';
part 'src/std_rules.dart';
part 'src/validation.dart';
part 'src/uid.dart';
part 'src/access.dart';
part 'src/fields.dart';
part 'src/collection.dart';
part 'src/query.dart';
part 'src/migrator.dart';

ConnectionPool db = new ConnectionPool(
  host: 'localhost',
  port: 3306,
  user: 'cado7',
  password: 'cado7',
  db: 'cado7',
  max: 10);

String secretKey = '8e34cfda3dcc9b5a5ac2ab9940ea02';
