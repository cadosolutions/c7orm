// Copyright (c) 2015, CadoSolutions. Please see LICENSE file for details.

library c7orm.test;

import 'package:test/test.dart';
import 'package:c7orm/c7orm.dart';

class PercentageRange {

  @Min(0)
  double min;

  @Max(100)
  double max;

  @Max(90)
  double get range => max - min;

  PercentageRange(this.min, this.max);

  @IsTrue("max should be greater than min")
  bool validate() {
    return this.max >= this.min;
  }
}

class PercentageRangeWithPeek extends PercentageRange {

  double peek;
  PercentageRangeWithPeek(double min, double max, this.peek) : super(min, max);

  @IsTrue("peek should be beetween min and max")
  bool validatePeek() {
      return this.max >= this.peek && this.peek >= this.min;
  }
}

void main() {

  group('Basic class validation', () {

      PercentageRange range;
      PercentageRangeWithPeek rangeWithPeek;

      setUp(() {
        range = new PercentageRange(10.0, 90.0);
        rangeWithPeek = new PercentageRangeWithPeek(0.0, 100.0, 50.0);
      });

      test('First Test', () {

        var result = new Validation(range);
        var result2 = new Validation(rangeWithPeek);
        result2.isValid();

        /*awesome.test = 10;
        var result2 = new Validator<ExampleEntity>(awesome);
        result2.isValid();*/

      });

    });
}
